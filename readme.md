# Example Client Side
This is a simple example to integrate OpenSSO at Client Side,  
means working only on user device/browser (static html).

### Concept
The goals is to make users can login via `OpenSSO login page` or via `Rest API`.  
So the flow is:
1. User trying to login
2. Save the token to localstorage so we can use the token to every page that require the credentials.
3. When the token has expired, users logout automatically.
4. Done.

Why we use localstorage?  
Because this example script is run in your static html (frontend only, no server backend required).


### Usage
To use this example script, make sure you have successfully running your OpenSSO.

**A. Using OpenSSO Login Page**  
Make sure you have already URL SSO registered.  
Then edit file `using-opensso-login.html`, at line 20.
```html
<!-- YOU MUST CHANGE THE URL LOGIN WITH YOUR OPENSSO -->
<a href="http://localhost:3000/sso/login/94ecfcbd02ec4ccb8509fc371fc320b9" class="w-100 btn btn-lg btn-primary">Single Sign On</a>
```

**B. Using API**  
Edit file `using-api,html`, at line 88
```js
// CHANGE URL WITH YOUR OPENSSO DOMAIN
var urlAPILogin = 'http://localhost:3000/api/user/login';
var urlAPIToken = 'http://localhost:3000/api/oauth/request_token';
// CHANGE WITH YOUR SSO KEY
var SSO_Key = '94ecfcbd02ec4ccb8509fc371fc320b9';
```

### How to get URL SSO

1. Login to your OpenSSO
2. Go to menu `My SSO`.
3. Add your new SSO.  
![](https://gitlab.com/nanowebdev/example/opensso/client-side/-/raw/master/img/tutorial-1.png)
4. Now you have `URL SSO` login page.
![](https://gitlab.com/nanowebdev/example/opensso/client-side/-/raw/master/img/tutorial-2.png)


### Cheatsheet
1. Get Token
```js
console.log(getToken());
```

2. Get User Info
```js
console.log(getUserInfo());
```

3. Parse JWT Token
```js
var token = getToken();
console.log(parseJWT(token));
```

### Need Help
Just chat with me via Telegram >> [https://t.me/aalfiann](https://t.me/aalfiann)
