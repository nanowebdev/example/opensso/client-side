'use strict';

// Define Localstorage
// to saving the token into user device/browser
var storage = new BrowserStorageClass(window.localStorage);

/**
 * Parse JWT
 * @param {string} token 
 * @return {object}
 */
function parseJWT (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
}

/**
 * Get url query parameter
 * @param {string} url 
 * @returns {mixed object|string}
 */
function getUrlParam (url) {
    if (url) {
        var query = url.split('?');
        if (query[1]) {
            var data = {};
            var queries = query[1].split('&');
            for (var i = 0; i < queries.length; i++) {
                var item = queries[i].split('=');
                data[item[0]] = decodeURIComponent(item[1]);
            }
            return data;
        }
    }
    return '';
}

/**
 * Remove Url Parameter
 * @param {string} url 
 * @param {string} parameter 
 * @returns {string}
 */
function removeUrlParam (url, parameter) {
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {
        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);
        // reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            // idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }
        url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
        return url;
    } else {
        return url;
    }
}
  
/**
 * Saving token to localstorage
 * @param {string} token 
 * @returns {bool}
 */
function saveToken(token) {
    // Extract data token
    var data = parseJWT(token);
    
    // Generate time
    var dt = new Date();
    var genTime = dt.getTime();
    var result = {
        token: token,
        generate: genTime,
        expire: (data.exp * 1000)
    };

    // Saving to localstorage
    try {
        storage.set('opensso_data', result);
        // remove token parameter on url
        var nurl = removeUrlParam(window.location.href, 'token');
        window.history.replaceState({}, document.title, nurl);
        return true;
    } catch (err) {
        console.log(err);
        return false;
    }
}

/**
 * Check token
 * @param {callback} _cb
 */
function checkToken (_cb) {
    var key = 'opensso_data';
    try {
        if (storage.has(key)) {
            var opensso_data = storage.get(key);
            if (new Date().getTime() > opensso_data.expire) {
                storage.remove(key);
                if (_cb && typeof _cb === 'function') {
                    return _cb('Token expired!!!', null);
                }
            }
            if (_cb && typeof _cb === 'function') {
                return _cb(null, true);
            }
        } else {
            if (_cb && typeof _cb === 'function') {
                return _cb('Token not available!!!', null);
            }
        }
    } catch (err) {
        if (_cb && typeof _cb === 'function') {
            return _cb(err, null);
        }
    }
}

/**
 * Get Token
 * @return {string}
 */
function getToken () {
    var key = 'opensso_data';
    if (storage.has(key)) {
      return storage.get(key).token;
    }
    return false;
}

/**
 * Get User Info
 * @returns {object}
 */
function getUserInfo() {
    return parseJWT(getToken());
}

/**
 * Logout
 * @param {callback} _cb
 */
function logout(_cb) {
    // just clear the token in localstorage
    try {
        storage.remove('opensso_data');
        if (_cb && typeof _cb === 'function') {
            return _cb(null, true);
        }
    } catch (err) {
        if (_cb && typeof _cb === 'function') {
            return _cb(err, null);
        }
    }
}
